# Webform Simplenews Handler

## INTRODUCTION

    This module provides a Webform Handler called “Submission Newsletter”
    that allows to link webform submission to one or more Simplenews
    newsletter subscriptions. This is useful if you want a form of
    newsletter subscription with more fields than the email address, maybe
    the name, region, etc.

## REQUIREMENTS

- The module requires the [Simplenews](https://www.drupal.org/project/simplenews) module as dependency.

## INSTALLATION

- Please use the Drupal 8 standard as `composer require
drupal/webform_simplenews_handler` or Administrator module installation.

## CONFIGURATION

- There is not any specific module configuration.